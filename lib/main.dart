import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:online_learning_app/screens/dashboard/dashboard_binding.dart';
import 'package:online_learning_app/screens/dashboard/dashboard_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: const Color.fromRGBO(42, 198, 165, 1),
        colorScheme: ColorScheme.fromSwatch().copyWith(
          primary: const Color.fromRGBO(42, 198, 165, 1),
          secondary: const Color.fromRGBO(252, 38, 38, 1),
          primaryContainer: const Color.fromRGBO(251, 179, 68, 1),
        ),
      ),
      initialRoute: '/dashboard',
      getPages: <GetPage>[
        GetPage(
          name: '/dashboard',
          page: () => const Dashboard(),
          binding: DashboardBinding(),
        ),
      ],
    );
  }
}
