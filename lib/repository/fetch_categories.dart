import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class FetchCategories {
  static final FetchCategories _singleton = FetchCategories._internal();

  factory FetchCategories() {
    return _singleton;
  }

  FetchCategories._internal();

  static Future getJson() async {
    return json.decode(
        await rootBundle.loadString('assets/data/categories.json')) as List;
  }
}
