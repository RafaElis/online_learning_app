import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class FetchPopularCourses {
  static final FetchPopularCourses _singleton = FetchPopularCourses._internal();

  factory FetchPopularCourses() {
    return _singleton;
  }

  FetchPopularCourses._internal();

  static Future getJson() async {
    return json.decode(
        await rootBundle.loadString('assets/data/courses_list_2.json')) as List;
  }
}
