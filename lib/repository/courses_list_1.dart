import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class CoursesList1 {
  static final CoursesList1 _singleton = CoursesList1._internal();

  factory CoursesList1() {
    return _singleton;
  }

  CoursesList1._internal();

  static Future getJson() async {
    return json.decode(
        await rootBundle.loadString('assets/data/courses_list_1.json')) as List;
  }
}
