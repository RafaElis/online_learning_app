class Course {
  final int id;
  final String title;
  final String duration;
  final String thumbnail;

  const Course({
    required this.id,
    required this.title,
    required this.duration,
    required this.thumbnail,
  });
}
