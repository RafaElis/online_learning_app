class Category {
  final int id;
  final String title;
  final String thumbnail;

  Category({
    required this.id,
    required this.title,
    required this.thumbnail,
  });
}
