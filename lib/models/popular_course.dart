class PopularCurse {
  final int id;
  final String title;
  final String duration;
  final String thumbnail;
  final String instructor;
  final double ratings;
  final int price;
  final bool isFavourite;

  const PopularCurse({
    required this.id,
    required this.title,
    required this.duration,
    required this.thumbnail,
    required this.instructor,
    required this.isFavourite,
    required this.price,
    required this.ratings,
  });

  // @override
  // String toString() {
  //   return 'PopularCurse{id: $id, title: $title, duration: $duration, thumbnail: $thumbnail, instructor: $instructor, ratings: $ratings, price: $price, isFavourite: $isFavourite}';
  // }
}
