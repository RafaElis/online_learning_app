import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_colors.dart';

class AnimatedSwitch extends StatefulWidget {
  final bool isActive;
  const AnimatedSwitch({
    required this.isActive,
    Key? key,
  }) : super(key: key);

  @override
  State<AnimatedSwitch> createState() => _AnimatedSwitchState();
}

class _AnimatedSwitchState extends State<AnimatedSwitch> {
  var isEnabled = true;
  final animationDuration = const Duration(milliseconds: 300);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        setState(() {
          isEnabled = !isEnabled;
        });
      },
      child: Stack(
        alignment: Alignment.center,
        clipBehavior: Clip.none,
        children: [
          AnimatedContainer(
            duration: animationDuration,
            width: 35,
            height: 5,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: isEnabled
                  ? AppColors.primary.withOpacity(.3)
                  : Colors.grey[400],
              // color: const Color(0xffF0FFFB),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.shade400,
                  spreadRadius: 2,
                  blurRadius: 10,
                ),
              ],
            ),
          ),
          AnimatedPositioned(
            duration: animationDuration,
            right: isEnabled ? -5 : 20,
            // right: isEnabled ? -5 : 20,
            // right: 0,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 2),
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 500),
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  border: Border.all(
                    color: isEnabled ? AppColors.primary : Colors.grey[400]!,
                    // color: AppColors.primary,
                    width: 3,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
