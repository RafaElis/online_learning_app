import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_colors.dart';
import 'package:online_learning_app/core/app_sizes.dart';

class Section extends StatefulWidget {
  final String title;
  final Function onPress;
  final Widget child;

  const Section({
    required this.title,
    required this.onPress,
    required this.child,
    Key? key,
  }) : super(key: key);

  @override
  State<Section> createState() => _SectionState();
}

class _SectionState extends State<Section> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: AppSize.padding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                widget.title,
                style: const TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              ),
              TextButton(
                onPressed: widget.onPress(),
                child: const Text(
                  'See all',
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                style: TextButton.styleFrom(
                  backgroundColor: AppColors.primary,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                ),
              ),
            ],
          ),
        ),
        widget.child,
      ],
    );
  }
}
