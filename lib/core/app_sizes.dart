class AppSize {
  static const double base = 8.0;
  static const double font = 14.0;
  static const double radius = 12.0;
  static const double padding = 24.0;
}
