class AppImages {
  static String get background_1 => 'assets/images/bg_1.png';
  static String get background_2 => 'assets/images/bg_2.png';
  static String get background_3 => 'assets/images/bg_3.png';
  static String get background_4 => 'assets/images/bg_4.png';
  static String get background_5 => 'assets/images/bg_5.png';
  static String get background_6 => 'assets/images/bg_6.png';
  static String get background_dark => 'assets/images/bg_dark.png';
  static String get background => 'assets/images/bg.png';

  static String get doc => 'assets/images/doc.png';
  static String get featured_bg_image => 'assets/images/featured_bg_image.png';
  static String get mobile_image => 'assets/images/mobile_image.png';
  static String get pdf => 'assets/images/pdf.png';
  static String get profile => 'assets/images/profile.png';
  static String get sketch => 'assets/images/sketch.png';
  static String get start_learning => 'assets/images/start_learning.png';

  static String get student_1 => 'assets/images/student_1.png';
  static String get student_2 => 'assets/images/student_2.png';
  static String get student_3 => 'assets/images/student_3.png';

  static String get thumbnail_1 => 'assets/images/thumbnail_1.png';
  static String get thumbnail_2 => 'assets/images/thumbnail_2.png';
  static String get thumbnail_3 => 'assets/images/thumbnail_3.png';
  static String get thumbnail_4 => 'assets/images/thumbnail_4.png';

  static String get work => 'assets/images/work.png';
}
