import 'package:flutter/material.dart';

class AppColors {
  static const Color primary = Color.fromRGBO(42, 198, 165, 1);
  static const Color primary_2 = Color.fromRGBO(251, 179, 68, 1.0);
  static const Color primary_3 = Color.fromRGBO(51, 53, 78, 1);
  static const Color secondary = Color.fromRGBO(252, 38, 38, 1);

  static const Color transparent = Colors.transparent;
  static const Color transparent_white_1 = Color.fromRGBO(255, 255, 255, 0.1);
  static const Color transparent_black_1 = Color.fromRGBO(0, 0, 0, 0.1);
  static const Color transparent_black_7 = Color.fromRGBO(0, 0, 0, 0.7);
}
