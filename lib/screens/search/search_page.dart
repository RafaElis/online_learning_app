import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:online_learning_app/core/app_sizes.dart';
import 'package:online_learning_app/screens/search/components/browse_categories.dart';
import 'package:online_learning_app/screens/search/search_controller.dart';
import 'package:online_learning_app/screens/search/components/top_searches.dart';

class Search extends StatelessWidget {
  const Search({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final SearchController searchController =
        Get.put(SearchController(), permanent: false);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(
                top: AppSize.base,
                bottom: AppSize.base,
                left: AppSize.padding,
                right: AppSize.padding,
              ),
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.shade400,
                    spreadRadius: 2,
                    blurRadius: 10,
                  ),
                ],
              ),
              child: Material(
                borderRadius: BorderRadius.circular(AppSize.radius),
                child: const TextField(
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.search, size: 30),
                    border: InputBorder.none,
                  ),
                ),
              ),
            ),
            const SizedBox(height: AppSize.base),
            TopSearches(searchController: searchController),
            const SizedBox(height: AppSize.padding),
            const BrowseCategories(),
          ],
        ),
      ),
    );
  }
}
