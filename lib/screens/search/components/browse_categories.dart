import 'package:flutter/material.dart';
import 'package:online_learning_app/components/section.dart';
import 'package:online_learning_app/core/app_sizes.dart';
import 'package:online_learning_app/models/category.dart';
import 'package:online_learning_app/repository/fetch_categories.dart';
import 'package:online_learning_app/screens/home/components/categories/category_card.dart';

class BrowseCategories extends StatelessWidget {
  const BrowseCategories({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Section(
      title: "Browse Categories",
      onPress: () {},
      child: FutureBuilder<List<Category>>(
        future: _generateCategoryCard(),
        initialData: const [],
        builder: (context, snapshot) {
          final List<Category> _categories = snapshot.data as List<Category>;
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return const Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.done:
              if (_categories.isNotEmpty) {
                return SizedBox(
                  child: GridView.builder(
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 1.20,
                      crossAxisSpacing: AppSize.padding,
                      mainAxisSpacing: AppSize.radius,
                    ),
                    padding: const EdgeInsets.only(
                      left: AppSize.padding,
                      right: AppSize.padding,
                      top: AppSize.radius,
                    ),
                    physics: const BouncingScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: _categories.length,
                    itemBuilder: (context, index) {
                      return CategoryCard(
                        id: _categories[index].id,
                        title: _categories[index].title,
                        thumbnail: _categories[index].thumbnail,
                      );
                    },
                  ),
                );
              }
          }
          return const Center(
            child: Text('No data'),
          );
        },
      ),
    );
  }

  Future<List<Category>> _generateCategoryCard() async {
    List<Category> children = [];
    final data = await FetchCategories.getJson();
    for (final item in data) {
      children.add(
        Category(
          id: item['id'],
          title: item['title'],
          thumbnail: item['thumbnail'],
        ),
      );
    }
    return children;
  }
}
