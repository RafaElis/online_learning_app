import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:online_learning_app/components/section.dart';
import 'package:online_learning_app/core/app_colors.dart';
import 'package:online_learning_app/core/app_sizes.dart';
import 'package:online_learning_app/screens/search/search_controller.dart';

class TopSearches extends StatelessWidget {
  const TopSearches({
    Key? key,
    required this.searchController,
  }) : super(key: key);

  final SearchController searchController;

  @override
  Widget build(BuildContext context) {
    final List topSearches = [
      {"id": 0, "label": "Sketch"},
      {"id": 1, "label": "Modeling"},
      {"id": 2, "label": "UI/UX"},
      {"id": 3, "label": "Web"},
      {"id": 4, "label": "Mobile"},
      {"id": 5, "label": "Animation"},
    ];
    return Section(
      title: "Top Searches",
      onPress: () {},
      child: SizedBox(
        height: 45,
        child: ListView.separated(
          padding: const EdgeInsets.only(
            left: AppSize.padding,
            right: AppSize.padding,
            top: AppSize.radius,
          ),
          shrinkWrap: true,
          separatorBuilder: (context, index) => const SizedBox(
            width: AppSize.base,
          ),
          physics: const BouncingScrollPhysics(),
          scrollDirection: Axis.horizontal,
          itemCount: 6,
          itemBuilder: (context, index) {
            return Obx(
              () => TextButton(
                onPressed: () => searchController.buttonIndex(index),
                child: Text(
                  topSearches[index]["label"],
                  style: TextStyle(
                    color: searchController.buttonIndex.value == index
                        ? Colors.white
                        : const Color(0xFF7F7F7F),
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                style: TextButton.styleFrom(
                  maximumSize: const Size(75, 45),
                  minimumSize: const Size(75, 45),
                  backgroundColor: searchController.buttonIndex.value == index
                      ? AppColors.primary
                      : const Color(0xFFE5E5E5),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(AppSize.base),
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
