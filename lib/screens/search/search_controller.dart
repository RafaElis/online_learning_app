import 'package:get/get.dart';

class SearchController extends GetxController {
  var buttonIndex = 0.obs;

  void changeButtonIndex(int index) {
    buttonIndex.value = index;
  }
}
