import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_icons.dart';

class ProfileHeader extends StatelessWidget {
  const ProfileHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        const Text(
          "Profile",
          style: TextStyle(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        Image.asset(
          AppIcons.sun,
          width: 25,
          height: 25,
          fit: BoxFit.contain,
          color: Colors.black,
        )
      ],
    );
  }
}
