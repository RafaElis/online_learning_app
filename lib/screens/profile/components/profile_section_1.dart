import 'package:flutter/material.dart';
import 'package:online_learning_app/screens/profile/components/profile_list.dart';

class ProfileSection1 extends StatelessWidget {
  const ProfileSection1({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> perfilSection1 = [
      {
        "icon": Icons.person_outline,
        "subtitle": "Nome",
        "title": "Rafael Elias",
      },
      {
        "icon": Icons.mail_outline,
        "subtitle": "E-mail",
        "title": "rafaelelias@teste.com",
      },
      {
        "icon": Icons.lock_outline,
        "subtitle": "Password",
        "title": "Updated 2 weeks ago",
      },
      {
        "icon": Icons.phone_outlined,
        "subtitle": "Contact Number",
        "title": "+55 (11) 99999-9999",
      },
    ];
    return ProfileList(perfilSection: perfilSection1);
  }
}
