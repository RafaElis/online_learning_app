import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_colors.dart';
import 'package:online_learning_app/core/app_images.dart';
import 'package:online_learning_app/core/app_sizes.dart';

class ProfileCard extends StatelessWidget {
  const ProfileCard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(AppSize.radius),
      margin: const EdgeInsets.symmetric(horizontal: AppSize.padding),
      decoration: BoxDecoration(
        color: AppColors.primary_3,
        borderRadius: BorderRadius.circular(AppSize.radius),
      ),
      child: LayoutBuilder(
        builder: (context, constraints) {
          return Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                clipBehavior: Clip.none,
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    width: 75,
                    height: 75,
                    decoration: BoxDecoration(
                      color: AppColors.primary_3,
                      image: DecorationImage(
                        image: AssetImage(AppImages.profile),
                        fit: BoxFit.cover,
                      ),
                      borderRadius: BorderRadius.circular(100),
                      border: Border.all(
                        color: Colors.white,
                        width: 3,
                      ),
                    ),
                    child: TextButton(
                      onPressed: () {},
                      style: ButtonStyle(
                        alignment: Alignment.bottomLeft,
                        overlayColor: MaterialStateColor.resolveWith(
                          (_) => const Color(0x3E9E9E9E),
                        ),
                        shape: MaterialStateProperty.resolveWith(
                          (_) => RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(100),
                          ),
                        ),
                        padding: MaterialStateProperty.resolveWith(
                          (states) => const EdgeInsets.symmetric(
                            horizontal: AppSize.radius,
                            vertical: AppSize.padding,
                          ),
                        ),
                      ),
                      child: const Text(""),
                    ),
                  ),
                  Positioned(
                    bottom: -6,
                    child: Material(
                      color: AppColors.primary,
                      borderRadius: BorderRadius.circular(100),
                      child: InkWell(
                        borderRadius: BorderRadius.circular(100),
                        onTap: () {},
                        child: Container(
                          padding: const EdgeInsets.all(4),
                          child: const Icon(
                            Icons.camera_alt,
                            size: 15,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(width: AppSize.base),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const Text(
                    "ByProgrammers",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(height: 5),
                  const Text(
                    "Full Stack Developer",
                    style: TextStyle(
                      fontSize: 13,
                      color: Colors.white,
                    ),
                  ),
                  const SizedBox(height: AppSize.radius),
                  SizedBox(
                    width: constraints.maxWidth -
                        75 -
                        AppSize.radius -
                        AppSize.base,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(AppSize.radius),
                      child: const LinearProgressIndicator(
                        value: 0.58,
                        backgroundColor: Colors.white,
                        minHeight: 8,
                      ),
                    ),
                  ),
                  const SizedBox(height: AppSize.base),
                  SizedBox(
                    width: constraints.maxWidth -
                        75 -
                        AppSize.radius -
                        AppSize.base,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const <Widget>[
                        Expanded(
                          flex: 1,
                          child: Text(
                            "Overall Progress",
                            style: TextStyle(
                              fontSize: 13,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        SizedBox(width: 50),
                        Text(
                          "58%",
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: AppSize.radius),
                  TextButton(
                    onPressed: () {},
                    style: ButtonStyle(
                      // alignment: Alignment.center,
                      backgroundColor: MaterialStateColor.resolveWith(
                        (_) => Colors.white,
                      ),
                      overlayColor: MaterialStateColor.resolveWith(
                        (_) => const Color(0x3E9E9E9E),
                      ),
                      shape: MaterialStateProperty.resolveWith(
                        (_) => RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: const <Widget>[
                        Icon(
                          Icons.add,
                          color: AppColors.primary,
                          size: 18,
                        ),
                        Text(
                          "Become Member",
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.primary,
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              )
            ],
          );
        },
      ),
    );
  }
}
