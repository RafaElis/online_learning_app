import 'package:flutter/material.dart';
import 'package:online_learning_app/components/animated_switch.dart';
import 'package:online_learning_app/core/app_colors.dart';

class ProfileListItem extends StatelessWidget {
  final Map<String, dynamic> item;

  const ProfileListItem({
    required this.item,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      horizontalTitleGap: 4,
      leading: Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: const Color(0xffF0FFFB),
        ),
        child: Icon(
          item["icon"],
          color: AppColors.primary,
        ),
      ),
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Visibility(
            visible: item["subtitle"].toString().isNotEmpty,
            child: Text(
              item["subtitle"],
              style: TextStyle(
                fontSize: 13,
                color: Colors.grey[400],
              ),
            ),
          ),
          Text(
            item["title"],
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
      // trailing: const AnimatedSwitch()
      trailing: item["isSwitchIcon"] != null
          ? AnimatedSwitch(
              isActive: item["isActive"],
            )
          : const Icon(
              Icons.arrow_forward_ios,
              color: Colors.black,
              size: 15,
            ),
    );
  }
}
