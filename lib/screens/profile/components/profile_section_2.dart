import 'package:flutter/material.dart';
import 'package:online_learning_app/screens/profile/components/profile_list.dart';

class ProfileSection2 extends StatelessWidget {
  const ProfileSection2({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Map<String, dynamic>> perfilSection = [
      {
        "icon": Icons.star_border,
        "subtitle": "",
        "title": "Pages",
      },
      {
        "icon": Icons.notifications_outlined,
        "subtitle": "",
        "title": "New Course Notifications",
        "isSwitchIcon": true,
        "isActive": false,
      },
      {
        "icon": Icons.timer_outlined,
        "subtitle": "",
        "title": "Study Reminder",
        "isSwitchIcon": true,
        "isActive": true,
      },
    ];
    return ProfileList(perfilSection: perfilSection);
  }
}
