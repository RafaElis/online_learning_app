import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_sizes.dart';
import 'package:online_learning_app/screens/profile/components/profile_list_item.dart';

class ProfileList extends StatelessWidget {
  final List<Map<String, dynamic>> perfilSection;
  const ProfileList({
    required this.perfilSection,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: AppSize.base,
        left: AppSize.padding,
        right: AppSize.padding,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(AppSize.radius),
        border: Border.all(
          color: Colors.grey[300]!,
          width: 1,
        ),
      ),
      child: ListView.separated(
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        separatorBuilder: (context, index) => Divider(
          height: AppSize.base,
          thickness: 1,
          indent: AppSize.radius,
          endIndent: AppSize.radius,
          color: Colors.grey[300]!,
        ),
        itemCount: perfilSection.length,
        itemBuilder: (context, index) {
          final item = perfilSection[index];
          return ProfileListItem(item: item);
        },
      ),
    );
  }
}
