import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_sizes.dart';
import 'package:online_learning_app/screens/profile/components/profile_header.dart';
import 'package:online_learning_app/screens/profile/components/profile_card.dart';
import 'package:online_learning_app/screens/profile/components/profile_section_1.dart';
import 'package:online_learning_app/screens/profile/components/profile_section_2.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const ProfileHeader(),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: const <Widget>[
            ProfileCard(),
            SizedBox(height: AppSize.base),
            ProfileSection1(),
            ProfileSection2(),
          ],
        ),
      ),
    );
  }
}
