import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:online_learning_app/core/app_colors.dart';
import 'package:online_learning_app/core/app_icons.dart';
import 'package:online_learning_app/core/app_sizes.dart';
import 'package:online_learning_app/screens/dashboard/dashboard_controller.dart';
import 'package:online_learning_app/screens/home/home_page.dart';
import 'package:online_learning_app/screens/profile/profile_page.dart';
import 'package:online_learning_app/screens/search/search_page.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    final DashboardController dashboardController =
        Get.put(DashboardController(), permanent: false);
    return Obx(
      () => Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: IndexedStack(
            index: dashboardController.tabIndex.value,
            children: const <Widget>[
              Home(),
              Search(),
              Profile(),
            ],
          ),
        ),
        bottomNavigationBar: buildBottomNavigationMenu(
          context,
          dashboardController,
        ),
      ),
    );
  }

  buildBottomNavigationMenu(
    BuildContext context,
    DashboardController dashboardController,
  ) {
    return Container(
      height: 70,
      margin: const EdgeInsets.symmetric(
        vertical: AppSize.radius,
        horizontal: AppSize.padding,
      ),
      decoration: BoxDecoration(
        color: AppColors.primary_3,
        borderRadius: BorderRadius.circular(AppSize.radius),
        boxShadow: const [
          BoxShadow(
            spreadRadius: 0.2,
            color: AppColors.primary_3,
            blurRadius: 15,
            offset: Offset(0, 2),
          ),
        ],
      ),
      child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return Stack(
            children: [
              AnimatedContainer(
                duration: const Duration(milliseconds: 300),
                curve: Curves.easeInOut,
                transform: Matrix4.translationValues(
                  constraints.maxWidth / 3 * dashboardController.tabIndex.value,
                  0,
                  0,
                ),
                width: constraints.minWidth / 3,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(AppSize.radius),
                ),
              ),
              Row(
                children: <Widget>[
                  ItemBar(
                    icon: Image.asset(
                      AppIcons.home,
                      height: 25,
                      width: 25,
                      fit: BoxFit.contain,
                    ),
                    title: "Home",
                    size: constraints.minWidth,
                    index: 0,
                  ),
                  ItemBar(
                    icon: Image.asset(
                      AppIcons.search,
                      height: 25,
                      width: 25,
                      fit: BoxFit.contain,
                    ),
                    title: "Search",
                    size: constraints.minWidth,
                    index: 1,
                  ),
                  ItemBar(
                    icon: Image.asset(
                      AppIcons.profile,
                      height: 25,
                      width: 25,
                      fit: BoxFit.contain,
                    ),
                    title: "Profile",
                    size: constraints.minWidth,
                    index: 2,
                  ),
                ],
              ),
            ],
          );
        },
      ),
    );
  }
}

class ItemBar extends StatelessWidget {
  final Image icon;
  final String title;
  final double size;
  final int index;

  const ItemBar({
    required this.icon,
    required this.title,
    required this.size,
    required this.index,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final DashboardController dashboardController =
        Get.put(DashboardController(), permanent: false);
    return Material(
      color: Colors.transparent,
      // color: isActive ? Theme.of(context).primaryColor : Colors.transparent,
      // borderRadius: BorderRadius.circular(AppSize.radius),
      child: InkWell(
        onTap: () => dashboardController.changeTabIndex(index),
        borderRadius: BorderRadius.circular(AppSize.radius),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppSize.radius),
          ),
          height: 85,
          width: size / 3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              icon,
              Padding(
                padding: const EdgeInsets.only(top: 3),
                child: Text(
                  title,
                  style: const TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
