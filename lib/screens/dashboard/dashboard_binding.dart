import 'package:get/get.dart';
import 'package:online_learning_app/screens/dashboard/dashboard_controller.dart';
import 'package:online_learning_app/screens/home/home_controller.dart';
import 'package:online_learning_app/screens/profile/profile_controller.dart';
import 'package:online_learning_app/screens/search/search_controller.dart';

class DashboardBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DashboardController>(() => DashboardController());
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<ProfileController>(() => ProfileController());
    Get.lazyPut<SearchController>(() => SearchController());
  }
}
