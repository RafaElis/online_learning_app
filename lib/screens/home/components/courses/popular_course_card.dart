import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_colors.dart';
import 'package:online_learning_app/core/app_icons.dart';
import 'package:online_learning_app/core/app_sizes.dart';

class PopularCurseCard extends StatelessWidget {
  final int id;
  final String title;
  final String duration;
  final String thumbnail;
  final String instructor;
  final double ratings;
  final int price;
  final bool isFavourite;

  const PopularCurseCard({
    required this.id,
    required this.title,
    required this.duration,
    required this.thumbnail,
    required this.instructor,
    required this.isFavourite,
    required this.price,
    required this.ratings,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {},
        borderRadius: BorderRadius.circular(AppSize.radius),
        child: Row(
          children: <Widget>[
            Container(
              width: 115,
              height: 115,
              padding: const EdgeInsets.all(5),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(thumbnail),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(AppSize.radius),
              ),
              child: Container(
                width: 20,
                height: 20,
                alignment: Alignment.topRight,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(AppSize.radius),
                  color: Colors.transparent,
                ),
                child: Material(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(AppSize.base),
                  child: InkWell(
                    onTap: () {},
                    borderRadius: BorderRadius.circular(AppSize.base),
                    child: Container(
                      padding: const EdgeInsets.all(5),
                      child: Image.asset(
                        AppIcons.heart,
                        color: isFavourite
                            ? AppColors.secondary
                            : const Color(0xFFC3C3C3),
                        width: 20,
                        height: 20,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(width: AppSize.base),
            Flexible(
              child: Column(
                children: <Widget>[
                  Text(
                    title,
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: AppSize.base),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "By $instructor",
                        style: TextStyle(
                          fontSize: 13,
                          color: Colors.grey[500],
                        ),
                      ),
                      const SizedBox(width: AppSize.base),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.access_time_sharp,
                            size: 18,
                            color: Colors.grey[500],
                          ),
                          const SizedBox(width: AppSize.base),
                          Text(
                            duration,
                            style: TextStyle(
                              fontSize: 13,
                              color: Colors.grey[500],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  const SizedBox(height: AppSize.base),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "R\$${price.toString()}.00",
                        style: const TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: AppColors.primary,
                        ),
                      ),
                      const SizedBox(width: AppSize.base),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Image.asset(
                            AppIcons.star,
                            width: 15,
                            height: 15,
                            fit: BoxFit.contain,
                          ),
                          const SizedBox(width: 5),
                          Text(
                            ratings.toString(),
                            style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: AppColors.primary_3,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
