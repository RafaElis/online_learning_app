import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_sizes.dart';
import 'package:online_learning_app/models/popular_course.dart';
import 'package:online_learning_app/repository/fetch_popular_courses.dart';
import 'package:online_learning_app/screens/home/components/courses/popular_course_card.dart';
import 'package:online_learning_app/components/section.dart';

class PopularCourses extends StatefulWidget {
  const PopularCourses({Key? key}) : super(key: key);

  @override
  State<PopularCourses> createState() => _PopularCoursesState();
}

class _PopularCoursesState extends State<PopularCourses> {
  @override
  Widget build(BuildContext context) {
    return Section(
      title: 'Popular Courses',
      onPress: () {},
      child: FutureBuilder<List<PopularCurse>>(
        future: _generateCoursesCard(),
        initialData: const [],
        builder: (context, snapshot) {
          final List<PopularCurse> _courses =
              snapshot.data as List<PopularCurse>;
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return const Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.done:
              if (_courses.isNotEmpty) {
                return ListView.separated(
                  physics: const NeverScrollableScrollPhysics(),
                  separatorBuilder: (context, index) => Divider(
                    height: AppSize.padding * 2,
                    color: Colors.grey[300],
                    thickness: 1,
                  ),
                  padding: const EdgeInsets.only(
                    left: AppSize.padding,
                    right: AppSize.padding,
                    top: AppSize.radius,
                  ),
                  shrinkWrap: true,
                  itemCount: _courses.length,
                  itemBuilder: (context, index) {
                    return PopularCurseCard(
                      id: _courses[index].id,
                      title: _courses[index].title,
                      thumbnail: _courses[index].thumbnail,
                      duration: _courses[index].duration,
                      price: _courses[index].price,
                      instructor: _courses[index].instructor,
                      isFavourite: _courses[index].isFavourite,
                      ratings: _courses[index].ratings,
                    );
                  },
                );
              }
          }
          return const Center(
            child: Text('No data'),
          );
        },
      ),
    );
  }

  Future<List<PopularCurse>> _generateCoursesCard() async {
    List<PopularCurse> children = [];
    final data = await FetchPopularCourses.getJson();
    for (final item in data) {
      children.add(
        PopularCurse(
          id: item['id'],
          title: item['title'],
          thumbnail: item['thumbnail'],
          duration: item['duration'],
          instructor: item['instructor'],
          price: item['price'],
          isFavourite: item['is_favourite'],
          ratings: item['ratings'],
        ),
      );
    }
    return children;
  }
}
