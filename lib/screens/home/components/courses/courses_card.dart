import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_colors.dart';
import 'package:online_learning_app/core/app_icons.dart';
import 'package:online_learning_app/core/app_sizes.dart';

class CoursesCard extends StatelessWidget {
  final int id;
  final String title;
  final String duration;
  final String thumbnail;

  const CoursesCard({
    required this.id,
    required this.title,
    required this.duration,
    required this.thumbnail,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {},
        borderRadius: BorderRadius.circular(AppSize.radius),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppSize.radius),
          ),
          width: 270,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 150,
                width: 270,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                      thumbnail,
                    ),
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.circular(AppSize.radius),
                ),
              ),
              const SizedBox(
                height: AppSize.radius,
              ),
              Row(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.all(AppSize.radius),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: AppColors.primary,
                    ),
                    child: Image.asset(
                      AppIcons.play,
                      width: 15,
                      height: 15,
                      fit: BoxFit.contain,
                    ),
                  ),
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: AppSize.radius,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            title,
                            style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                          const SizedBox(height: AppSize.base),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Image.asset(
                                AppIcons.time,
                                width: 20,
                                height: 20,
                                color: Colors.grey[500],
                              ),
                              const SizedBox(width: AppSize.base),
                              Text(
                                duration,
                                style: TextStyle(
                                  fontSize: 13,
                                  color: Colors.grey[500],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
