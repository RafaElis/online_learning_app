import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_sizes.dart';
import 'package:online_learning_app/models/course.dart';
import 'package:online_learning_app/repository/courses_list_1.dart';
import 'package:online_learning_app/screens/home/components/courses/courses_card.dart';

class Courses extends StatefulWidget {
  const Courses({Key? key}) : super(key: key);

  @override
  State<Courses> createState() => _CoursesState();
}

class _CoursesState extends State<Courses> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Course>>(
      future: _generateCoursesCard(),
      initialData: const [],
      builder: (context, snapshot) {
        final List<Course> _courses = snapshot.data as List<Course>;
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return const Center(
              child: CircularProgressIndicator(),
            );
          case ConnectionState.done:
            if (_courses.isNotEmpty) {
              return SizedBox(
                height: 250,
                child: ListView.separated(
                  separatorBuilder: (context, index) => const SizedBox(
                    width: AppSize.radius,
                  ),
                  padding:
                      const EdgeInsets.symmetric(horizontal: AppSize.padding),
                  physics: const BouncingScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: _courses.length,
                  itemBuilder: (context, index) {
                    return CoursesCard(
                      id: _courses[index].id,
                      title: _courses[index].title,
                      duration: _courses[index].duration,
                      thumbnail: _courses[index].thumbnail,
                    );
                  },
                ),
              );
            }
        }
        return const Center(
          child: Text('No data'),
        );
      },
    );
  }

  Future<List<Course>> _generateCoursesCard() async {
    List<Course> children = [];
    final data = await CoursesList1.getJson();
    for (final item in data) {
      children.add(
        Course(
          id: item['id'],
          title: item['title'],
          duration: item['duration'],
          thumbnail: item['thumbnail'],
        ),
      );
    }
    return children;
  }
}
