import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_sizes.dart';

class CategoryCard extends StatelessWidget {
  final int id;
  final String title;
  final String thumbnail;
  const CategoryCard({
    required this.id,
    required this.title,
    required this.thumbnail,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 160,
      height: 200,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(AppSize.radius),
        image: DecorationImage(
          image: AssetImage(thumbnail),
          fit: BoxFit.cover,
        ),
      ),
      child: TextButton(
        onPressed: () {},
        style: ButtonStyle(
          alignment: Alignment.bottomLeft,
          overlayColor: MaterialStateColor.resolveWith(
            (_) => const Color(0x3E9E9E9E),
          ),
          shape: MaterialStateProperty.resolveWith(
            (_) => RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(AppSize.radius),
            ),
          ),
          padding: MaterialStateProperty.resolveWith(
            (states) => const EdgeInsets.symmetric(
              horizontal: AppSize.radius,
              vertical: AppSize.padding,
            ),
          ),
        ),
        child: Text(
          title,
          style: const TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 22,
          ),
        ),
      ),
    );
  }
}
