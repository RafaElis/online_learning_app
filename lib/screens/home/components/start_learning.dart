import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_images.dart';
import 'package:online_learning_app/core/app_sizes.dart';

class StartLearning extends StatelessWidget {
  const StartLearning({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      margin: const EdgeInsets.only(
        top: AppSize.padding,
        bottom: AppSize.padding,
      ),
      padding: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(AppImages.featured_bg_image),
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.circular(AppSize.radius),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Text(
            "How to",
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          const Text(
            "Make your brand more visible with our checklist",
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            ),
          ),
          const SizedBox(height: AppSize.radius),
          const Text(
            "By Scott Harris",
            style: TextStyle(
              fontSize: 13,
              color: Colors.white,
            ),
          ),
          const SizedBox(height: AppSize.padding),
          Image.asset(
            AppImages.start_learning,
            width: double.maxFinite,
            height: 115,
          ),
          TextButton(
            onPressed: () {},
            child: const Text(
              "Start Learning",
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            style: TextButton.styleFrom(
              backgroundColor: Colors.white,
              padding: const EdgeInsets.symmetric(
                horizontal: AppSize.padding,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
