import 'package:flutter/material.dart';
import 'package:online_learning_app/core/app_icons.dart';
import 'package:online_learning_app/core/app_sizes.dart';
import 'package:online_learning_app/screens/home/components/categories/categories.dart';
import 'package:online_learning_app/screens/home/components/courses/courses.dart';
import 'package:online_learning_app/screens/home/components/courses/popular_courses.dart';
import 'package:online_learning_app/screens/home/components/start_learning.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Text(
                  "Hello, ByProgrammers!",
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                Text(
                  "Terça-Feira, 01 de Nov de 2022",
                  style: TextStyle(
                    fontSize: 13,
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
            Material(
              color: Colors.transparent,
              child: InkWell(
                onTap: () {},
                borderRadius: BorderRadius.circular(100),
                child: Container(
                  padding: const EdgeInsets.all(8),
                  child: Image.asset(
                    AppIcons.notification,
                    width: 25,
                    height: 25,
                    fit: BoxFit.contain,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.symmetric(
                horizontal: AppSize.padding,
              ),
              child: StartLearning(),
            ),
            const Courses(),
            Divider(
              height: AppSize.padding,
              color: Colors.grey[300],
              thickness: 1,
            ),
            const Categories(),
            const SizedBox(height: 20),
            const PopularCourses(),
            // Section(
            //   title: 'Popular Courses',
            //   onPress: () {},
            // ),
          ],
        ),
      ),
    );
  }
}
